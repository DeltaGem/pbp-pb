# SPDX-Licence-Identifier: CC0-1.0
{pkgs ? import /home/user/src/nixpkgs {}}: let
  petitboot = pkgs.stdenv.mkDerivation rec {
    version = "git";
    name = "petitboot";
    src = /home/user/src/petitboot;
    nativeBuildInputs = with pkgs; [autoreconfHook pkg-config flex bison];
    buildInputs = with pkgs; [ncurses eudev (lvm2.overrideAttrs (x:{
        installTargets = ["install_device-mapper"];
        buildFlags = ["device-mapper"];
      }))
    ];
    postPatch = ''
      echo 'echo "${version}"' > version.sh
    '';
    configureFlags = ["--enable-busybox" "--localstatedir=/var" "--sharedstatedir=/com" "--disable-dependency-tracking"];
  };
in {
  inherit petitboot;
}
