self: super: {
  libselinux = null; # super.libselinux.override {enablePython = false; python = {sitePackages="";};};
  #utillinux = super.utillinux.override {systemd = null;};
  #lvm2 = super.lvm2.override {systemd = self.eudev;};
  systemd = null;
  gnome2 = super.gnome2.overrideScope' (self: super: {gtk-doc = null;}); #'
  glib = (super.glib.override {gtk-doc = null;}).overrideAttrs (x: {
    mesonFlags = self.lib.remove "-Dgtk_doc=true" x.mesonFlags ++ ["-Dselinux=disabled"];
  });
  gtk_doc = null;
  libusb1 = super.libusb1.override {systemd = self.eudev;};
  coreutils = super.coreutils.overrideAttrs (x: {doCheck = false;});
}
