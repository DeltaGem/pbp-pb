# SPDX-Licence-Identifier: CC0-1.0
{pkgs ? import /*<nixos>*/ /home/user/src/nixpkgs {
  localSystem = /*if builtins.currentSystem == "aarch64-linux" then null else*/ (import <nixos/lib>).systems.examples.aarch64-multiplatform-musl;
  overlays = [(import ./overlay.nix)];
}}:
let
  p = pkgs;
in rec {
  petitboot = (import ./petitboot.nix {inherit pkgs;}).petitboot;
  env = p.buildEnv {
    name = "env";
    paths = with p; [
      busybox
      eudev
      kexectools
      petitboot
    ];
  };
  init = p.writeScript "init" ''
    #!/bin/sh
    echo "Hello world!"
    mount -t proc none /proc
    mount -t sysfs none /sys
    mount -t devtmpfs none /dev
    mount -t tmpfs none /tmp
    mount -t tmpfs none /run
    mount -t tmpfs none /var
    mount -t tmpfs none /com
    ls /dev
    ${p.eudev}/bin/udevd -D >>/tmp/udev.log 2>&1 &
    ${p.eudev}/bin/udevadm trigger -t subsystems -c add &
    ${p.eudev}/bin/udevadm trigger -c add &
    echo 400000 >/sys/devices/system/cpu/cpufreq/policy4/scaling_min_freq
  '';
  dir = p.runCommandNoCC "root-dir" {preferLocalBuild = true;} ''
    mkdir $out
    cd $out
    mkdir -p sys proc dev etc/udev/rules.d tmp run etc/init.d var com
    cp ${init} etc/init.d/rcS
    cp ${./00-blkid.rules} etc/udev/rules.d/00-hwdb.rules
    ${p.buildPackages.eudev}/bin/udevadm hwdb --update --root=$(pwd)
    ls etc/udev
    for x in bin lib ; do ln -s $(realpath ${env}/$x --relative-to=/) $x; done
    ln -s /bin sbin
    cat ${p.closureInfo {rootPaths = [env];}}/store-paths
    cp -prd --parents $(cat ${p.closureInfo {rootPaths = [env];}}/store-paths) .
  '';
  squashfs = p.runCommandNoCC "squashfs" {} "${p.buildPackages.squashfsTools}/bin/mksquashfs ${dir}/ $out -all-root -comp zstd";
  cpio = p.runCommandNoCC "initcpio" {preferLocalBuild = true;} "cd ${dir} && find * -print0 | sort -z | ${p.buildPackages.cpio}/bin/cpio -o -H newc -R +0:+0 --reproducible --null | gzip -n >$out";
}
